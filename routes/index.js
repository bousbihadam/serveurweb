var express = require('express');
var app = express(); 
var cors = require('cors');
app.use(cors());
var jwt = require('jsonwebtoken');
var User = require('../models/users');
var Trajet = require('../models/trajets');
var TrajetType   = require('../models/trajetsType');
var bodyParser = require('body-parser');

 // parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/covoiturage');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('connecter à la base mongodb');
});

var apiTrajets = require('../api/apiTrajets');
var apiUsers = require('../api/apiUsers');
 
app.get('/allTrajets',apiTrajets.getAllTrajets);

app.get('/trajets/:id',apiTrajets.getTrajetById);

app.get('/lastTrajets',apiTrajets.getLastTrajets);

app.post('/addTrajet',apiTrajets.addTrajet);
 
app.post('/connexion', apiUsers.connexion);
 
app.post('/inscription', apiUsers.inscription);



module.exports = app;