/* API users*/

var jwt = require('jsonwebtoken');
var User = require('../models/users');
var Trajet = require('../models/trajets');

exports.connexion = function(req,res){
 
    console.log(req.body);
    User.findOne({ 'email': req.body.email,password: req.body.password }, function(err,user) {
        console.log(user);
         if(user){
            var payload = {
                userID: user._id
            }
            var token = jwt.sign(payload, 'pdwMEAN', {
                expiresIn: 3600  
            });
           
            res.json({
                userID: user._id,
                allowed: true,
                token: token
            });
         }
         else{
            let data = { 
                    allowed:false
            };
            console.log(data);
            res.json(data);
          
        }
        });
}

exports.inscription = function(req, res) {
    console.log(req.body.email);
   let newUser = {
        email: req.body.email,
        password: req.body.password,
        nom: req.body.nom,
        prenom:req.body.prenom,
        telephone:req.body.telephone,
        ville: req.body.ville,
        note: 0,
        admin: false,
        dateInscription: new Date() ,
      }
      console.log(newUser);
   User.create(newUser, function (err, user) {
        if (err) {
            let data = { 
                allowed:false
            };
            console.log(err);
        res.json(data);
        } else {
            var payload = {
                userID: user._id
            }
            var token = jwt.sign(payload, 'pdwMEAN', {
                expiresIn: 3600  
            });
    
            res.json({
                userID: user._id,
                allowed: true,
                token: token
            });
    
        }
      });
    }


      
