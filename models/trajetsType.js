var mongoose = require('mongoose');
var TrajetsTypeSchema = new mongoose.Schema({
    depart: { type: String },
    arrivee: { type: String },
    prixMax: { type: Number }
  });
var TrajetsType = mongoose.model('trajetsType', TrajetsTypeSchema);
module.exports = TrajetsType;