var mongoose = require('mongoose');
var UserSchema = new mongoose.Schema({
  email: { type: String, unique: true },
  password: { type: String },
  nom: { type: String },
  prenom: { type: String },
  telephone: { type: String },
  ville: { type: String },
  note: { type: Number },
  admin: { type: Boolean },
  dateInscription: { type: Date, default: Date.now },

});
var User = mongoose.model('User', UserSchema);
module.exports = User;
