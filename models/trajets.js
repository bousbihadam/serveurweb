var mongoose = require('mongoose');
var TrajetSchema = new mongoose.Schema({
  userID: { type: String }, //mongoose.Schema.Types.ObjectId,
  depart: { type: String },
  arrivee: { type: String },
  rdvDepart: { type: String },
  rdvDepose: { type: String },
  date: { type: Date, default: Date.now },
  distance: { type: Number },
  prix: { type: Number }
});
var Trajet = mongoose.model('trajets', TrajetSchema);
module.exports = Trajet;


  